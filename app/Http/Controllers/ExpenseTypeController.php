<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Redirect;
use Illuminate\Http\Request;
use App\ExpenseType;

class ExpenseTypeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
    	$types = ExpenseType::all();

        return view('types.index')->with('types', $types);
    }

    public function create()
    {
        return view('types.create');
    }

    public function store()
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'type_name'  	=> 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        if ($validator->fails()) {
            return Redirect::to('expensetypes')
                ->withErrors($validator)
                ->withInput(Input::except('password'));
        } else {
            // store
            $type = new ExpenseType;
            $type->type_name 		= Input::get('type_name');
            $type->save();

            return Redirect::to('expensetypes');
        }
    }

    public function destroy($id)
    {
        $type = ExpenseType::findOrFail($id);

	    $type->delete();

	    return redirect()->route('expensetypes.index');
    }
}
