<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use Charts;
use App\Expense;
use App\ExpenseType;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use ConsoleTVs\Charts\Builder\Database;
use Illuminate\Support\Facades\Input;

class ChartController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
	public function index()
	{	

		$monthSort = Input::get('month');

		$expenseSumsByType = DB::table('expenses')
			->whereMonth('created', '=', date($monthSort))
			->groupBy('type')
			->select('type', DB::raw('sum(price) as sumedPrice'))
			->get();

		$chart = Charts::create('pie', 'highcharts')
			->title('Statistika')
			->elementLabel("Total")
			->dimensions(1000, 600)
			->responsive(false)
			->labels($expenseSumsByType->pluck('type'))
			->values($expenseSumsByType->pluck('sumedPrice'))
			->responsive('true');


	//dump($test);die;

		return view('charts.index', ['chart' => $chart]);
		
	}
}
