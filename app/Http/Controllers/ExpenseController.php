<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\http\Controllers\ExpenseController;
use App\Expense;
use App\ExpenseType;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;
use Session;
use Redirect;

class ExpenseController extends Controller
{
	public function __construct()
    {
        $this->middleware('auth');
    }
    
	public function index()
	{
    	// get all the nerds
		$expenses = Expense::all();

        // load the view and pass the nerds
		return view('expenses.index')->with('expenses', $expenses);
	}

	public function create()
	{
		$items = ExpenseType::all(['id', 'type_name']);

    	//dump($items);die;

		$types = [];

		foreach ($items as $key => $value) {
			$types[$value->type_name] = $value->type_name;
		}
		return view('expenses.create', compact('types'));
        //return view('expenses.create');
	}

	public function store()
	{
        // validate
        // read more on validation at http://laravel.com/docs/validation
		$rules = array(
			'created' 	=> 'required',
			'name'  	=> 'required',
			'price' 	=> 'required|numeric',
			'type'      => 'required'
			);
		$validator = Validator::make(Input::all(), $rules);

        // process the login
		if ($validator->fails()) {
			return Redirect::to('expenses/create')
			->withErrors($validator)
			->withInput(Input::except('password'));
		} else {
            // store
			$expense = new Expense;
			$expense->created 		= Input::get('created');
			$expense->name     		= Input::get('name');
			$expense->price 		= Input::get('price');
			$expense->type      	= Input::get('type');
			$expense->description 	= Input::get('description');
			$expense->save();

            // redirect
			Session::flash('message', 'Expense Created!');
			return Redirect::to('expenses');
		}
	}

	public function edit($id)
	{
		$items = ExpenseType::all(['id', 'type_name']);
		$expense = Expense::find($id);

		$types = [];

		foreach ($items as $key => $value) {
			$types[$value->type_name] = $value->type_name;
		}

		return view('expenses.edit', compact('expense', 'types'));
	}


	public function show($id)
	{
	
	}



	public function update($id)
	{
        // validate
        // read more on validation at http://laravel.com/docs/validation
		$rules = array(
			'created' 	=> 'required',
			'name'  	=> 'required',
			'price' 	=> 'required|numeric',
			'type'      => 'required'
			);
		$validator = Validator::make(Input::all(), $rules);

        // process the login
		if ($validator->fails()) {
			return Redirect::to('expenses/{id}/edit')
			->withErrors($validator)
			->withInput(Input::except('password'));
		} else {
            // store
			$expense = Expense::find($id);
			$expense->created 		= Input::get('created');
			$expense->name     		= Input::get('name');
			$expense->price 		= Input::get('price');
			$expense->type      	= Input::get('type');
			$expense->description 	= Input::get('description');
			$expense->save();

            // redirect
			Session::flash('message', 'Expense Created!');
			return Redirect::to('expenses');
		}
	}

	public function destroy($id)
	{
		$expense = Expense::findOrFail($id);

		$expense->delete();

		Session::flash('flash_message', 'Ištrinta!');

		return redirect()->route('expenses.index');
	}
}
