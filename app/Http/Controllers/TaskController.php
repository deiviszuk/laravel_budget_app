<?php
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\http\Controllers\ExpenseController;
use App\Expense;
use App\Task;
use App\ExpenseType;
use Session;
use Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Input;

class TaskController extends Controller
{

	public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
    	$tasks = Task::all();

        return view('tasks.index')->with('tasks', $tasks);
    }

    public function store()
    {
        // validate
        // read more on validation at http://laravel.com/docs/validation
        $rules = array(
            'task'  	=> 'required',
        );
        $validator = Validator::make(Input::all(), $rules);

        // process the login
        
            $task = new Task;
            $task->task = Input::get('task');
            $task->save();


            return Redirect::to('tasks');
        
    }

    public function destroy($id)
    {
        $task = Task::findOrFail($id);

	    $task->delete();

	    return redirect()->route('tasks.index');
    }



}
