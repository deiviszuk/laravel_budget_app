<!doctype html>
<html>
<head>

    <meta charset="utf-8">
    <title>Budget APP</title>
<!-- Latest compiled and minified CSS -->

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<!-- Bootstrap Core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">

<!-- Custom CSS -->
<link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="/css/simple-sidebar.css" rel="stylesheet">
    
    

<!--     Date Picker     -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <!--<link rel="stylesheet" href="/resources/demos/style.css">-->
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
      $( function() {
        $( "#datepicker" ).datepicker({
          changeMonth: true,
          changeYear: true,
          dateFormat: 'yy-mm-dd'
      });
    } );
    </script>
<!-- End Date Picker -->
<script src="{{ asset('js/app.js') }}"></script>
{!! Charts::assets() !!}

</head>
<body>

    <!--  Header  -->
    <div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
                <li class="sidebar-brand">                </li>
                <li><a href="{{URL::to('expenses')}}">Biudžetas</a></li>
                <li><a href="{{URL::to('tasks')}}">Planavimas</a></li>
            </ul>
        </div>
        <!-- /#sidebar-wrapper -->
        <!--   Header  -->
        @include('layouts.authnav')
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div id="main" class="row">
                    
                    @yield('content')

                </div>
            </div>
        </div>

        <!-- jQuery -->
        <script src="/js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="/js/bootstrap.min.js"></script>

        <!-- Menu Toggle Script -->
        <script>
            $("#menu-toggle").click(function(e) {
                e.preventDefault();
                $("#wrapper").toggleClass("toggled");
            });
        </script>

        <script src="//code.jquery.com/jquery-1.10.2.js"></script>
        <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
        <script>
          $(function() {
            $( "#datepicker" ).datepicker();
        });
    </script>

</body>
</html>