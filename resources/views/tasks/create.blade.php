@extends('layouts.main')

@section('content')
<div class="container-fluid">
<div class="container">
		<!-- if there are creation errors, they will show here -->
		{{ Html::ul($errors->all()) }}

		{{ Form::open(array('url' => 'expensetypes')) }}

		<div class="form-group">
			{{ Form::label('type_name', 'Pavadinimas') }}
			{{ Form::text('type_name', Input::old('name'), array('class' => 'form-control')) }}
		</div>

		{{ Form::submit('Patvirtinti', array('class' => 'btn btn-primary')) }}

		{{ Form::close() }}
	</div>
</div>
@stop