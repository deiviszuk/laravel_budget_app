@extends('layouts.main')

@section('content')
<div class="container-fluid" style="width: 70%">

	<span class="container">
		{{ Html::ul($errors->all()) }}

		{{ Form::open(array('url' => 'tasks')) }}

		<div class="form-group">
			{{ Form::label('task', 'Naujas Užduotis') }}
			{{ Form::text('task', Input::old('name'), array('class' => 'form-control')) }}
		</div>

		{{ Form::submit('Įtraukti', array('class' => 'btn btn-primary')) }}

		{{ Form::close() }}
	</span>

		<table class="table table-striped">
			<thead>
				<tr>
					<td>Užduotis</td>
				</tr>
			</thead>
			<tbody>
				@foreach($tasks as $value)
				<tr>
					<td>{{ $value->task }}</td>
					<!-- Valdymo mygtukai -->
					<td>
						<!-- istrinimas -->
						{{ Form::open(array('url' => 'tasks/' . $value->id, 'class' => 'pull-right')) }}
						{{ Form::hidden('_method', 'DELETE') }}
						{{ Form::submit('Atlikta', array('class' => 'btn btn-success')) }}
						{{ Form::close() }}

					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>

@stop