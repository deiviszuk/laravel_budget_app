@extends('layouts.main')
@section('content')
<nav class="navbar navbar-inverse">
	<ul class="nav navbar-nav">
		<li><a href="{{ URL::to('expenses') }}">Visos Išlaidos</a></li>
		<li><a href="{{ URL::to('expensetypes') }}">Išlaidų tipai</a></li>
		<li><a href="{{ URL::to('charts') }}">Statistika</a></li>
	</ul>
</nav>
<div class="container-fluid">

<div class="container">

		{{ Html::ul($errors->all()) }}
		{{ Form::open(array('url' => 'charts')) }}

		<div class="form-group">
			{{ Form::label('month', 'Mėnesis') }}
			{{ Form::select('month', array(
			'1' => 'Sausis', 
			'2' => 'Vasaris',
			'3' => 'Kovas',
			'4' => 'Balandis',
			'5' => 'Gegužė',
			'6' => 'Birželis',
			'7' => 'Liepa',
			'8' => 'Rugpjūtis',
			'9' => 'Rugsėjis',
			'10' => 'Spalis',
			'11' => 'Lapkritis',
			'12' => 'Gruodis',
			),  Input::old('month'), array('class' => 'form-control'))  }}
		</div>
		{{ Form::submit('Patvirtinti', array('class' => 'btn btn-primary')) }}
		{{ Form::close() }}
	<div class="container">
		{!! $chart->render() !!}
	</div>
	</div>
</div>

</div>
</body>
</html>
@endsection