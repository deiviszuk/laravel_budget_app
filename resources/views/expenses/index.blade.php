@extends('layouts.main')
@section('content')
<nav class="navbar navbar-inverse">
	<ul class="nav navbar-nav">
		<li><a href="{{ URL::to('expenses') }}">Visos Išlaidos</a></li>
		<li><a href="{{ URL::to('expensetypes') }}">Išlaidų tipai</a></li>
		<li><a href="{{ URL::to('charts') }}">Statistika</a></li>
		</ul>
	</nav>
	<div class="container-fluid">
		<table class="table table-striped">
		<a class="btn btn-success glyphicon glyphicon-plus", href= "{{ URL::to('expenses/create') }}", aria-hidden="true"></a>
			<thead>
				<tr>
					<td>ID</td>
					<td>Data</td>
					<td>Pavadinimas</td>
					<td>Suma</td>
					<td>Tipas</td>
					<td>Aprašymas</td>
				</tr>
			</thead>
			<tbody>
				@foreach($expenses as $value)
				<tr>
					<td>{{ $value->id }}</td>
					<td>{{ $value->created }}</td>
					<td>{{ $value->name }}</td>
					<td>{{ $value->price }}</td>
					<td>{{ $value->type }}</td>
					<td>{{ $value->description }}</td>

					<!-- Valdymo mygtukai -->
					<td>
						<!-- show the nerd (uses the show method found at GET /nerds/{id} -->
						<a class="btn btn-small btn-success" href="{{ URL::to('expenses/' . $value->id . '/edit') }}">Keisti</a>

						<!-- edit this nerd (uses the edit method found at GET /nerds/{id}/edit -->
						{{ Form::open(array('url' => 'expenses/' . $value->id, 'class' => 'pull-right')) }}
						{{ Form::hidden('_method', 'DELETE') }}
						{{ Form::submit('Ištrinti', array('class' => 'btn btn-danger')) }}
						{{ Form::close() }}

					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>

</div>
</body>
</html>
@stop