@extends('layouts.main')
@section('content')
<nav class="navbar navbar-inverse">
	<ul class="nav navbar-nav">
		<li><a href="{{ URL::to('expenses') }}">Visos Išlaidos</a></li>
		<li><a href="{{ URL::to('expensetypes') }}">Išlaidų tipai</a></li>
		<li><a href="{{ URL::to('charts') }}">Statistika</a></li>
		</ul>
	</nav>
	<div class="container">
		<!-- if there are creation errors, they will show here -->
		{{ Html::ul($errors->all()) }}

		{{ Form::open(array('url' => 'expenses')) }}

		<div>
			{{ Form::label('date', 'Data') }}
			{{ Form::text('created', Input::old('created'), array('class' => 'form-control', 'id' => 'datepicker')) }}
		</div>

		<div>
			{{ Form::label('name', 'Pavadinimas') }}
			{{ Form::text('name', Input::old('name'), array('class' => 'form-control')) }}
		</div>

		<div>
			{{ Form::label('price', 'Kaina') }}
			{{ Form::text('price', Input::old('price'), array('class' => 'form-control')) }}
		</div>

		<div>
			{{ Form::label('type', 'Tipas') }}
			{{ Form::select('type', $types, Input::old('type'), array('class' => 'form-control')) }}
		</div>

		<div>
			{{ Form::label('description', 'Aprašymas') }}
			{{ Form::text('description', Input::old('description'), array('class' => 'form-control')) }}
		</div>

		{{ Form::submit('Patvirtinti', array('class' => 'btn btn-primary')) }}

		{{ Form::close() }}
	</div>
	@endsection