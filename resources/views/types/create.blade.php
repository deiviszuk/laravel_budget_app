@extends('layouts.main')
@section('content')
<nav class="navbar navbar-inverse">
	<ul class="nav navbar-nav">
		<li><a href="{{ URL::to('expenses') }}">Visos Išlaidos</a></li>
		<li><a href="{{ URL::to('expensetypes') }}">Išlaidų tipai</a></li>
		<li><a href="{{ URL::to('charts') }}">Statistika</a></li>
		</ul>
	</nav>
	<div class="container">
		<!-- if there are creation errors, they will show here -->
		{{ Html::ul($errors->all()) }}

		{{ Form::open(array('url' => 'expensetypes')) }}

		<div class="form-group">
			{{ Form::label('type_name', 'Pavadinimas') }}
			{{ Form::text('type_name', Input::old('name'), array('class' => 'form-control')) }}
		</div>

		{{ Form::submit('Patvirtinti', array('class' => 'btn btn-primary')) }}

		{{ Form::close() }}
	</div>
	@endsection