@extends('layouts.main')
@section('content')
    <nav class="navbar navbar-inverse">
	<ul class="nav navbar-nav">
		<li><a href="{{ URL::to('expenses') }}">Visos Išlaidos</a></li>
		<li><a href="{{ URL::to('expensetypes') }}">Išlaidų tipai</a></li>
		<li><a href="{{ URL::to('charts') }}">Statistika</a></li>
	</ul>
	</nav>
	<div class="container-fluid">

	<span class="container">
		{{ Html::ul($errors->all()) }}

		{{ Form::open(array('url' => 'expensetypes')) }}

		<div class="form-group">
			{{ Form::label('type_name', 'Naujas Tipas') }}
			{{ Form::text('type_name', Input::old('name'), array('class' => 'form-control')) }}
		</div>

		{{ Form::submit('Įtraukti', array('class' => 'btn btn-primary')) }}

		{{ Form::close() }}
	</span>

		<table class="table table-striped">
			<thead>
				<tr>
					<td>ID</td>
					<td>Pavadinimas</td>
				</tr>
			</thead>
			<tbody>
				@foreach($types as $value)
				<tr>
					<td>{{ $value->id }}</td>
					<td>{{ $value->type_name }}</td>
					<!-- Valdymo mygtukai -->
					<td>
						<!-- istrinimas -->
						{{ Form::open(array('url' => 'expensetypes/' . $value->id, 'class' => 'pull-right')) }}
						{{ Form::hidden('_method', 'DELETE') }}
						{{ Form::submit('Ištrinti', array('class' => 'btn btn-danger')) }}
						{{ Form::close() }}

					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
@stop