<?php

Auth::routes();
Route::get('/', 'ExpenseController@index');

Route::resource('/expenses', 'ExpenseController');
Route::resource('/expensetypes', 'ExpenseTypeController');
Route::resource('/charts', 'ChartController');
Route::post('/charts', 'ChartController@index');
route::resource('/tasks', 'TaskController');

?>

